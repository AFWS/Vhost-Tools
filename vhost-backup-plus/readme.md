# APPLICATION TITLE: VHost Backup Plus

 ( *Inspired by "Kubik-Rubik's WordPress Backup Script", which seems to now be defunct.* )

 **AUTHOR:**     Jim S. Smith

 **COPYRIGHT:**  (c)2023 - A FRESH WEB SOLUTION

 **LICENSE:**    GPL 2.0 or > ( *with proper attribution please* )

 **VERSION:**    1.3.0b, *Updated:* 2023/12/15

 **USE:**        Backup complete virtual hosts w/ websites, database(s), and their configuration files.

 **DESCRIPTION:**

    A CLI script to backup a complete virtual host w/ CMS-driven website(s) and its/their database(s).

    Designed to also be run as a "CRON"-task, for scheduled backups. This script will even archive any of its
    server vhost-configuration files and, if found to be running, its PHP-FPM config files. A complete
    solution to backup the entire vhost with its necessary configuration files. Great for use on development
    servers!

 **NOTE:**  Must run this application as sudo!

 **REQUIREMENTS:**

 1. Should use PHP version 7.3 or newer. ( *Not yet tested for PHP 8.* ),
 2. PHP-MySQLi library, installed and enabled,
 3. PHP-PGSQL (PostgreSQL) library, installed and enabled,
 4. PHP-JSON extension library, installed and enabled,
 5. Ability to use the "system()", "escapeshellcmd", and "escapeshellarg" functions (usually available in PHP-CLI),
 6. Coded to work with Apache2 SAPI with PHP-FPM(optional - with mod_fastcgi enabled),
 7. File-system functions: 'file_get_contents()' and 'file_put_contents()' must also be usable,
 8. PHP Zip-class library, installed and enabled.

 **OTHER NOTES:**

    To make sure only root users (or those with 'sudo' privileges) can access this utility:
        This script should be placed in /usr/sbin or /usr/share/sbin, and chmod'd to '0750'.

    *This script can also be used in a cron task for scheduled backups!*
