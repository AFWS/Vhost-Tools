# APPLICATION TITLE: VHost Delete

 **AUTHOR:**     Jim S. Smith

 **COPYRIGHT:**  (c)2023 - A FRESH WEB SOLUTION

 **LICENSE:**    GPL 2.0 or > ( *with proper attribution please* )

 **VERSION:**    1.0.1d, *Updated:* 2023/12/16

 **USE:**        Delete/Remove virtual hosts.

 **DESCRIPTION:**

    A CLI script to completely remove virtual hosts and their config files from
    a system running Apache2 SAPI ( and, optionally, its PHP-FPM config ).

 **NOTE:**  Must run this application as sudo!

 **REQUIREMENTS:**

 1. PHP version 7.3 or > is highly advised. ( *Not yet tested for PHP 8.* )
 2. Apache2 is expected to be installed and running,
 3. PHP-FPM is optional, but highly recommended,
 4. Ability to use PHP's "system()" function,
 5. File-system functions: 'file_get_contents()' and 'file_put_contents()' must be usable,

 **OTHER NOTES:**

    To make sure only root users (or those with 'sudo' privileges) can access this utility:
        This script should be placed in /usr/sbin or /usr/share/sbin, and chmod'd to '0750'.
