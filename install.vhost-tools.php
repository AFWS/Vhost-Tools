#!/usr/bin/env php
<?php   //  ۞ text{ encoding:utf-8; bom:no; linebreaks:unix; tabs:4; }  ۞ //

/*
# APPLICATION TITLE: VHost-Tools Installer.

 **AUTHOR:**    Jim S. Smith

 **COPYRIGHT:** (c)2023 - A FRESH WEB SOLUTION

 **LICENSE:**   GPL 2.0 or > (*With proper attribution please.*)

 **VERSION:**   1.0.0b, *Updated:* 2023/12/17

 **DESCRIPTION:** A CLI script to copy the vhost tools to: '/usr/sbin' and set file perms accordingly.

 **NOTE:**  Must run this application as sudo!

 **REQUIREMENTS:** (Basic, for all the vhost tools):

 1. Should use PHP-CLI version 7.3 or newer. ( *Not yet tested for PHP 8.* ),
 2. Ability to use the "system()", "escapeshellcmd", and "escapeshellarg" functions (usually available in PHP-CLI),
 3. Coded to run with Apache2 SAPI with PHP-FPM(optional - with mod_fastcgi enabled),
 4. File-system functions: 'file_get_contents()' and 'file_put_contents()' must also be usable,
 5. PHP Zip-class library, installed and enabled,
 6. The PHP POSIX library of functions.

 **OTHER NOTES:**

    To make sure only root users (*or those with 'sudo' privileges*) can access these utilities:
        Scripts are placed in '/usr/sbin' and chmod'd to '0750'.

*/


//  Colored Error messages!
function post_errors( $this_message ) {
    fwrite( STDERR, <<< _TEXT_
\033[06;31m\033[1m FATAL ERROR: \033[0m \033[17;31m\033[1m $this_message \033[0m


_TEXT_
    );

    die( 1 );
}

//  Send shell commands, and retrieve output results.
function send_to_shell( $cmd_str = '' ) {
    ob_start();
    system( "$cmd_str" );
    $return_code = trim( ob_get_clean() );

    if ( empty( $return_code ) ) {
        return 0;
        }

    return $return_code;
}

//	Check to see if this instance is being executed as "root" user.
function auth_check() {
    if ( send_to_shell( 'id -u' ) ) {
        post_errors( "Need to be 'root'-user in order to run this task!" );
        }

    return true;
}

//	Custom function to check for the existence of directories.
function dir_exists( $_dir_path = '' ) {
	return ( ! empty( $_dir_path ) && is_dir( $_dir_path ) && is_readable( $_dir_path ) && is_writable( $_dir_path ) );
}


auth_check();

echo <<< _SCREEN_TEXT_

   YOU ARE ABOUT TO INSTALL THESE VHOST TOOLS ONTO YOUR SYSTEM.

   They will be made to only be available to "root" and other "sudo'ers".

   Do you wish to proceed? ? ?


_SCREEN_TEXT_;


if ( strtolower( readline( "   ( Just type 'yes' and then enter. )  " ) === 'yes' ) ) {
	echo "\n\n";

	$_file_array = [
		'vhost-backup-plus/vhost-backup',
		'vhost-delete/vhost-delete',
		'vhost-generator/vhost-gen',
		'vhost-restore/vhost-restore',
		];

	$_install_errors = false;

	foreach ( $_file_array as $_this_file ) {
		$_dest_filename = basename( "./$_this_file" );

		if ( file_exists( "./$_this_file" ) && is_readable( "./$_this_file" ) ) {
			echo "Installing: '$_dest_filename' to: '/usr/sbin'.\n";
			send_to_shell( "cp ./$_this_file /usr/sbin/$_dest_filename ; chmod 0750 /usr/sbin/$_dest_filename ; " );
			}

		else {
			echo "* * OOPS! File './$_this_file' was not found or unreadable! * *\n";
			$_install_errors .= "'$_dest_filename' was not installed!\n";
			}
		}
	}

else {
	die( "Action aborted by user.\n\n" );
	}

if ( $_install_errors ) {
	echo <<< _ERROR_TEXT_


\033[06;31m\033[1m INSTALLATION ERRORS: \033[0m

\033[17;31m\033[1m $_install_errors \033[0m


_ERROR_TEXT_;
	}

else {
	echo "\nThe Vhost Tools have been successfully installed.\nYou can check for them using 'sudo which <this-vhost-script>'.\n\n";
	}

die(0);


