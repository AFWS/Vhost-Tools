Version: 2.1.5d - Build: 20231228

CHANGES/UPDATES:

* CHANGED:

  Further optimized the file-type REGEXES in the vhost "htdata" generation functions.



Version: 2.1.4c - Build: 20231218

CHANGES/UPDATES:

* CHANGED:

  1) Converted inline script-header documentation to markdown-format inside of a comment container.

  2) Standardized the appearance of the "test" pages.

  3) On the "index.html" page, added list of hyperlinks to the test-scripts, and reformatted the page content.


* REMOVED: The function which generates the "old-style" Python test-page script -> No longer needed.


* ADDED: New "readme.md" file, in proper markdown format, to the distribution.



Version: 2.1.3c - Build: 20231213

CHANGES/UPDATES:

* ADDED: Adds "ht-config" directive to ensure that canonical name of URL is used, and NOT
         the vhost's IP address. ( Redirects a "302" from the IP address in the browser's
         "address bar" over to the actual "Server-Name" URL ( the "canonical name" ).



Version: 2.1.2c - Build: 20231206

CHANGES/UPDATES:

* ADDED: Now creates a "test.rb" file in the new vhost, for testing if Ruby is installed and working.



Version: 2.1.2b - Build: 20231127

CHANGES/UPDATES:

* CHANGED: Optimized some "coding" in one of the PHP-FPM Apache2 directives -

	- Consolidated the "IF" and "FILESMATCH" blocks into one "IF", in the "<IfModule mod_proxy_fcgi.c>" container.


* ADDED: A few custom file access functions for better error-trapping.



Version: 2.1.1e - Build: 20231117

CHANGES/UPDATES:

* ADDED (Cosmetics): Now date-stamps all configuration files and new "hosts" entries.


OTHER CHANGES:

* Minor coding clean-up.



Version: 2.1.0c - Build: 20221120

CHANGES/UPDATES:

1) Corrected an error in blank "username" and "groupname" being accepted,

2) Now requires at least a username to be specified,

3) Both user and group must have UID/GID greater than the set defaults (which are set up in two defined constants near the beginning of the script),

4) Two new functions to find and then parse for numeric ID's, the /etc/passwd file, and /etc/group file,

5) Added code to check the UID/GID of the given user and group,


OTHER CHANGES:

1. Now, "www-data" is no longer the set default for user and group names. These must be specified (user name at minimum),

2. "Help" is now displayed using "fwrite( STDOUT, $help_text )"-method, instead of just "echo" or "die",

3. Other minor coding improvements/clean-up.

4. Corrected some misstatements in the "help"-text.

