# APPLICATION TITLE: VHost Generator

 ( *Based on an original idea at: https://gist.github.com/fideloper/2710970* )

 **AUTHOR:**     Jim S. Smith

 **COPYRIGHT:**  (c)2023 - A FRESH WEB SOLUTION

 **LICENSE:**    GPL 2.0 or > ( *with proper attribution please* )

 **VERSION:**    2.1.5d, *Updated:* 2023/12/28

 **USE:**        Easily-create new virtual hosts.

 **DESCRIPTION:**

    A CLI script to help a computer-user create new starter virtual hosts on a
    system running Apache2 SAPI, and PHP. This script creates the necessary server-
    configuration files for the virtual host and, if to be used, creates the
    PHP-FPM "pool" configuration file, enabling the newly-created vhost to run
    PHP files under a different user from the web-server.

    This script also creates the necessary "hosts" file entry to enable the new
    vhost.

    After the setup is complete ( *and hopefully successful* ), the web-server ( *and
    if necessary, the PHP-FPM daemon* ) are restarted.

 **NOTE:**  Must run this application as sudo!

 **REQUIREMENTS:**

 1. PHP-CLI version 7.3 or > is highly advised. ( *Tested up to PHP 8.2.* )
 2. Apache2 installed and running,
 3. PHP-FPM is optional, but highly recommended,
 4. Ability to use PHP's "system()" function,
 5. File-system functions: 'file_get_contents()' and 'file_put_contents()' must be usable,
 6. Ability to use PHP's POSIX functions,

 **OTHER NOTES:**

    To make sure only root users (or those with 'sudo' privileges) can access this utility:
        This script should be placed in /usr/sbin or /usr/share/sbin, and chmod'd to '0750'.
