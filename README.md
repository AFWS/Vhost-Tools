# Vhost-Tools

## IMPORTANT: This "repo" houses the most current Vhost Tools versions.

*The other, earlier forms of this project are no longer being actively maintained, and should no longer be depended upon for updates, maintenance, error-corrections, etc!*

This supersedes the "*New Vhost 2*" and "*New Vhost Scripts*" repositories especially.

**DESCRIPTION:**  This collection of "tools" is designed for each to be interoperable with all the others. Whereas "*Vhost Generator*" is recommended for generating any virtual host instances - which the other tools will be expected to be operable with, the other tools **may** be able to also be used with vhosts which were not created solely by "*Vhost Generator*", *but this is NOT recommended!*

**Included:**  A handy "installer script": *scripted in PHP-CLI*, which will copy all four vhost-tools to your secured system binary folder ( "/usr/sbin" ) and set very limiting file-permissions access to them ( like: "*chmod 0750*" ). - This is for the safety and security of your system, as this limits access to these tools from unauthorized users.

**NOTE:**  The "installer" will overwrite older copies of these scripts, *regardless of their versions*!

**ADDITIONAL NOTE(S):**

* Each of the vhost tools has its own "user manual" in PDF file-type. Hopefully, most questions will be adequately answered within these user manuals.

* These vhost tools are as "user-friendly" as can be coded, while also providing a high degree of utility.

* Recommended for any developer who works with one or many vhosts, or similars, on their own development platforms!

* This collection of scripts is intended to simplify the management and use of virtual hosts on almost any computer system set up to provide an environment very similar to a web server.

**ASIDE:** *If there is a call for it, I may opt to update the PERL, BASH, and other script versions at a later time.*

