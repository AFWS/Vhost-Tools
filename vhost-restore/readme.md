# APPLICATION TITLE: VHost Restore

 **AUTHOR:**     Jim S. Smith

 **COPYRIGHT:**  (c)2023 - A FRESH WEB SOLUTION

 **LICENSE:**    GPL 2.0 or > ( *with proper attribution please* )

 **VERSION:**    1.1.1b, *Updated:* 2023/12/28

 **USE:**        Restore archived virtual hosts w/ websites and their database(s).

 **DESCRIPTION:** A CLI script to restore a complete virtual-host backup on a server.

 **NOTE:**  Must run this application as sudo!

 **REQUIREMENTS:**

 1. Should use PHP version 7.3 or newer. ( *Not yet tested for PHP 8.* ),
 2. Ability to use the "system()", "escapeshellcmd", and "escapeshellarg" functions (usually available in PHP-CLI),
 3. Coded to run with Apache2 SAPI with PHP-FPM ( *optional - with mod_fastcgi enabled* ),
 4. File-system functions: 'file_get_contents()' and 'file_put_contents()' must also be usable,
 5. PHP Zip-class library, installed and enabled,

 **OTHER NOTES:**

    To make sure only root users (or those with 'sudo' privileges) can access this utility:
        This script should be placed in /usr/sbin or /usr/share/sbin, and chmod'd to '0750'.

    This script can also detect differences in PHP versions, and alters the restored configs to the
    current PHP version on a system!
